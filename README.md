#Overview

This source code demonstrates the usage of Lightning Lookup Component. 
Code Sample:

```sh
    <c:Typeahead fieldLabel="Account" 
                 objName="Account" 
                 selectedId="{!v.contactWrap.accLkp.id}" 
                 selectedLabel="{!v.contactWrap.accLkp.label}" 
                 lookupId="accLkp" 
                 isRequired="true"
    /> 
```

We have to pass values to following attributes from the container component to the lookup component "Typeahead" (src/aura/Typeahead/):

Attribute  | Details
------------- | -------------
fieldLabel  | Label to be displayed as part of the form element.
objName  | API name of object for which lookup is for.
selectedId  | Value of lookup/MD field (Eg: objContact.AccountId)
selectedLabel  | Value of lookup/MD field's Name (Eg: objContact.Account.Name)
lookupId  | Unique Id to lookup, useful when we fire ALJSErrorEvent from container of this component. Setting ALJSErrorEvent.aljsId attribute to lookupId while firing event from Container component + verifying aljsId via getParam in Typeahead component helps to properly handle validation in case of multiple Typeahead components are involved in the same App or Component.
isRequired | Displays red color * symbol in markup
searchPlaceholder | Placeholder text to be displayed in search box.

#Demo Lightning Component

#### On load of quick action, empty account lookup (Because the existing contact has empty account) ####
![Load](https://bitbucket.org/repo/KrnAypA/images/1084202347-L1.PNG)

#### Start typing in search box ####
![Search](https://bitbucket.org/repo/KrnAypA/images/1159899737-L2.PNG)

#### Select an account ####
![Select](https://bitbucket.org/repo/KrnAypA/images/2395044332-L3.PNG)

#### Start typing, but don't select any account and click "Update" button  => Invalid field validation ####
![Invalid](https://bitbucket.org/repo/KrnAypA/images/1114686053-L4.PNG)

#### Click close icon on search and click "Update" button => Required field validation ####
![Required](https://bitbucket.org/repo/KrnAypA/images/4256945363-L5.PNG)

#### Select an account and successful update ####
![Success](https://bitbucket.org/repo/KrnAypA/images/1965784082-L33.PNG)

#Typeahead functionality#

This component has SobjectLookupController as its controller. 
####On load of the component:####
1. doInit() fetches ICON details from server side for the respective object name. But, in fallback mode (When no object tab is present for the object or object tab is not added into an app), displays an account icon for the lookup. 
2. If respective reference field has value (selectedId and selectedLabel attributes values), component displays it in search input box with close icon. Else, displays empty search box so that user can start searching for records. 

####Search:####
1. When user focuses on empty search box, controller fetches 500 records from server side ORDER BY Name without any filter and displays in lookup menu. 
2. As user starts typing, controller fetches 500 records from server side based on search term (Filter: records whose Name contains the search term) and updates lookup menu accordingly. 
selectedId and selectedLabel will have the proper binding at any time.

####Validation:####
Make sure to pass unique id to lookupId attribute of Typeahead.
Validation is handled from the container component via an application event (ALJSErrorEvent).
Container component has to register for this event. Eg:
```sh
<aura:registerEvent name="lookupErrorReq" type="c:ALJSErrorEvent"/>
```
Typeahead component handles the event:
```sh
<aura:handler event="c:ALJSErrorEvent" action="{!c.handlerError}" />
```
Let's look into the code snippet of container component's controller(ContactDisplayController):
```sh
validateInputs: function(component)
{
	var contactWrap = component.get("v.contactWrap");
	// ...... Other code
	// if Id is empty
	if($A.util.isEmpty(contactWrap.accLkp.id))
	{
		isValid = false;
		/* if Id is empty but label is not empty => user has typed something in search box 
		 * but not selected any value or not closed the searchbox, throw error */
		if(!$A.util.isEmpty(contactWrap.accLkp.label))
		{
			this.fireErrorEvent("accLkp", "Please select value from lookup menu");
		}
		/* if both Id & label are empty => required field validation */
		else
		{
			this.fireErrorEvent("accLkp", "You must enter a value");
		}
	} 
	return(isValid);
},
fireErrorEvent : function(aljsId, errorMsg)
{
	var errorEvt = $A.get("e.c:ALJSErrorEvent");
	errorEvt.setParams({
		"errorMsg" : errorMsg, 
		"aljsId" : aljsId
	});     
	errorEvt.fire(); 
},
```

Here "accLkp" is value passed to lookupId attribute. When you have to throw error for the Typeahead component, pass "errorMsg" and aljsId (lookupId attribute's value) to the event and fire().

Once the Typeahead component recieves the notification, handlerError action is performed:
```sh
handlerError : function(component, event, helper) 
{
	// verify lookup Id before adding error
	if(event.getParam("aljsId") == component.get("v.lookupId"))
	{
		var elem = component.find("formElem");
		$A.util.addClass(elem, 'slds-has-error'); 
		component.set("v.errorMsg", event.getParam("errorMsg"));
	}
}
```
if(event.getParam("aljsId") == component.get("v.lookupId")) line checks if passed aljsId matches lookupId and adds the error.

#### Technical Highlights ####
* This component updates contact with entered last name and account.
* Icon for the lookup is dynamically fetched. But, in fallback mode (When no object tab is present for the object or object tab is not added into an app), displaying an account icon.
* We have tried using lightning:icon instead of separate component (c:svgIcon) to display SVGs. But, it disrupts the UI little bit and we ended up using svgIcon component. This is the only reason behind explicitly including static resource(slds_214) for SLDS.

#Installation

If you are using an IDE like eclipse with the force.com plugin, then copy the class and its associated xml files to your project directory/src/classes. 
Copy the lightning components, event, app and its xml files to the /src/aura.
Copy the static resource and its xml files to the /src/staticresources.

Or you can manually create artifacts via setup (UI)

Create a new Action on Contact for the "ContactDisplay" Lightning Component and update the page layout to add the action under "Salesforce1 & Lightning Actions". Switch to LEX and click on created action button on Contact Detail Page. Reference : https://trailhead.salesforce.com/en/modules/lex_javascript_button_migration/units/javascript_buttons_to_lightning_actions