/**
	ClassName   : ContactController
	Description : Handles fetch and updation of contact.
*/
public with sharing class ContactController 
{
    /**  
        @MethodName : getContact
        @Param      : Contact Id 
        @Description: Retrieve contact details (LastName and Account)
    **/  
    @AuraEnabled
    public static String getContact(String conId)
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'id', 'lastname', 'accountid' 
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Contact', true, true, false);
        
        // prepare wrapper
        ContactWrap objWrap = new ContactWrap();
        if(String.isNotBlank(conId))
        {
            List<Contact> lstCon = [SELECT Id, LastName, AccountId, Account.Name
                                	FROM Contact 
                                	WHERE Id =: conId];
            if(!lstCon.isEmpty())
            {
                objWrap.objCon = lstCon[0];
                /** For the reference fields using another wrapper here:
					For the null lkp/MD field-values (eg: account is a lookup field, so it can be null), 
					if we try to retrieve objContact.Account.Name, objContact.AccountId at ligthing comp level, it won't work. Using wrapper with 
					JSON.serializePretty with suppressApexObjectNulls = false while returning contact data helps in maintaining the binding.
				**/
				objWrap.accLkp = new SobjectLookupController.LookupWrapper(lstCon[0].Account.Name, lstCon[0].AccountId);
            }
        }
        /* If we have null values for a field, we cannot get corresponding field as an object's property in Lightning. 
         * That is why, binding becomes difficult at lightning side.
        	Even if we use wrapper class instance and return it, same issue exists on Lightning component side for null values. 
        Hence, using wrapper class + JSON.serializePretty(); with suppressApexObjectNulls = false will retain the field as properties for null values.
        Parsing this json string at the Lightning component level gives proper result.
        */
        return JSON.serializePretty(objWrap, false);  
    }
    
    /**  
        @MethodName : updateContact
        @Param      : JSON Contact wrapper
        @Description: Update contact details (LastName and Account)
    **/  
    @AuraEnabled
    public static String updateContact(String jsonCon) 
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'lastname', 'accountid' 
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Contact', false, false, true);
        
        String errorMsg = '';
        ContactWrap objWrap = (ContactWrap)JSON.deserialize(jsonCon, ContactWrap.class);
         
        Contact objCon = new Contact();
        objCon = objWrap.objCon; 
        
        // attach the account Id
        objCon.AccountId = String.isBlank(objWrap.accLkp.id) ? null : Id.valueOf(objWrap.accLkp.id);

        try 
        { 
            update objCon;
        }  
        catch(Exception ex) 
        {
            throw new AuraHandledException(ex.getMessage()); 
        }
        return objCon.Id;
    }
    
    public class ContactWrap
    {
        @AuraEnabled 
        public SobjectLookupController.LookupWrapper accLkp {get;set;}
        @AuraEnabled
        public Contact objCon {get;set;}
        public ContactWrap()
        {
            
        } 
    }
}